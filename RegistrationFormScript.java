package Workshop;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class RegistrationFormScript {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("file:///C:/Users/Balaji-Lap/Desktop/New%20folder/testregistration/index.html");
		driver.findElement(By.name("userid")).sendKeys("1234567");
		driver.findElement(By.name("passid")).sendKeys("asdfghj");
		driver.findElement(By.name("username")).sendKeys("asddfghh");
		driver.findElement(By.name("address")).sendKeys("ClassicPg");
		driver.findElement(By.name("zip")).sendKeys("12345");
		WebElement dropdown = driver.findElement(By.name("country"));
		Select selectDropdown  = new Select(dropdown);
		selectDropdown.selectByValue("DZ");
		selectDropdown.selectByValue("Default");
		selectDropdown.selectByValue("AD");
		selectDropdown.selectByValue("AS");
		selectDropdown.selectByValue("AF");
		selectDropdown.selectByValue("AL");
		driver.findElement(By.name("email")).sendKeys("hello@gmail.com");
		driver.findElement(By.name("msex")).click();
		driver.findElement(By.name("fsex")).click();
		driver.findElement(By.name("nonen")).click();
		driver.findElement(By.name("desc")).sendKeys("Hello");
		driver.findElement(By.name("submit")).click();
		String popupText = driver.switchTo().alert().getText();
		System.out.println(popupText);
		if (popupText.equals("Form Succesfully Submitted")) {
			driver.switchTo().alert().accept();
		}
		driver.manage().window().maximize();
		driver.quit();
	}
}
